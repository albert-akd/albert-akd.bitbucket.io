/**/
function addval(){
    let elements = document.querySelectorAll("#calculette>div.calc");
    let s1 = document.getElementById("s1");
    let s2 = document.getElementById("s2");

    elements.forEach((item) => {
        item.addEventListener("click",()=>{
            if(item.id=="ans"){
                let last_val = get_lastvalue();
                clear_screen(s1,s2,last_val)
            }
            else{
                    clear_screen(s1,s2,item.textContent);
            }
        });
    });
}

/*effacer l'ecran */
function clear_screen(s1,s2,txt){

    if(s1.textContent.length !== 0 && s2.textContent.length !== 0)
    {
        supp();
        s1.append(txt);
        s1.innerHTML = add_mark(s1);
    }
    else{
            
        s1.append(txt);
        s1.innerHTML = add_mark(s1);
    }
}


/*Enregistrer la dernier valeure Valide*/
function set_lastvalue(val){
    if(!isNaN(val) && isFinite(val))
    {
            localStorage.setItem("ans", val);
    }
}



function to_localstorage(a){

    var a = [];
    a = JSON.parse(localStorage.getItem('ans_')) || [];
    a.push(data);
    localStorage.setItem('ans', JSON.stringify(a));
}


/*recuperer la derniere valeur*/
function get_lastvalue(){
    let val = localStorage.getItem("ans");
    return parseFloat(val);
}

/*enlever dernier chiffre*/
function supp_last(){
    let last = document.getElementById("s1").textContent;
    document.getElementById("s1").textContent=last.slice(0,-1);

}
/*supprimer les entrees*/ 
function supp()
{
    let screen_elements = document.querySelectorAll("#screen>.scr");
    screen_elements.forEach((item)=>{
        item.textContent="";
    })
}
/*calcul des sommes*/
function calculate()
{
    let text = document.getElementById("s1").textContent;
    try{
            /*Sans Eval*/
        let withouteval = (new Function('return '+text)())
        set_lastvalue(withouteval);
        document.getElementById("s2").textContent = withouteval;

    }
    catch(e){
        document.getElementById("s1").textContent="Syntax";
        document.getElementById("s2").textContent="Error";
    }
    
}
/*executer la premiere fonction*/
addval();

/**Addenventlistener */
/*document.getElementById("s1").addEventListener("DOMCharacterDataModified",()=>{console.log("helllo")}); reverifier */
document.getElementById("ce").addEventListener("click",supp_last);
document.getElementById("del").addEventListener("click",supp);
document.getElementById("eq").addEventListener('click',calculate);

/*COlorer les caracteres speciaux dans la calculatrice a corriger*/
function add_mark(elem)
{
    let retour ="";
	const reg = /([()+/*-])(?![<mark>][/mark])/g;
    const subst = `<mark>$1</mark>`;
    retour = elem.innerHTML.replaceAll(reg,subst);
    return retour;
    
}
